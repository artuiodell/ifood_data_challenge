# iFood Data Architect Test Solution

Repo containing the solution for the Data Architect Challenge.

## Repo architecture
```
├── README.md
├── src/
│   └── py_scripts/
│         ├── `1.load_data_on_datalake_raw.py`
│         ├── `2.create_stage_order.py`
│         └── ...
│   └── jobs_configs/
│         ├── `1.load_data_on_datalake_raw.json`
│         ├── `2.create_stage_order.json`
│         └── ... _same structure from above folder_
├── requirements.txt
└── .gitignore
```

### Explaining each folder
#### `src/py_scripts/`:

- Here for 2 things:
  - 1. So that people who are going to evaluate can read the solution implemented, 
  - 2. To import on Databricks (or other tool) and execute the pipeline. If you do that, make sure to execute following the order in the files names.

#### `src/jobs_configs/`:

- Folder that stores the json configs to run ETLs on Databricks.
- The first one is configured to run at 00:00 from Mon to Fri, and each other to start 20mins later
    [00:20, 00:40, ...]

## Env information
Regardless of where you will run the code, follow these settings:
- Java Version: 1.8.0_302 (Azul Systems, Inc.)
- Scala Version: 2.12.10
- Spark Version: 3.1.2

Still, it is recommended to run the Notebooks in Databricks

## Documentation
To understand more about the code, infra architecture, and technical debts,
feel free to access [this](https://artuiodell.atlassian.net/wiki/spaces/IFOODDATAC/pages/262152/iFood+Data+Architect+Challenge+Solution) page on Confluence, which is in PT-BR.
