import pyspark
import pyspark_anonymizer

from pyspark.sql import SparkSession
import pyspark.sql.functions as spark_functions
from pyspark.sql.functions import col, to_utc_timestamp, year, month, dayofmonth


def get_aws_params():
    s3_datalake_path = "s3://ifood-challenge-arthur-datalake/"
    
    s3_raw_path = s3_datalake_path + "raw/"
    s3_stage_path = s3_datalake_path + "stage/"
    s3_trusted_path = s3_datalake_path + "trusted/"
    
    return {
        "aws_access_key": "AKIAVF4EEUDKTPDXRA7Y",
        "aws_secret_key": "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",
        "aws_s3_raw_consumer_path": s3_raw_path + "consumer/",
        "aws_s3_raw_order_path": s3_raw_path + "order/",
        "aws_s3_raw_restaurant_path": s3_raw_path + "restaurant/",
        "aws_s3_raw_status_path": s3_raw_path + "status/",
        "aws_s3_stage_order_path": s3_stage_path + "stage_order/",
        "aws_s3_trusted_item_path": s3_trusted_path + "item/",
        "aws_s3_trusted_orders_path": s3_trusted_path + "orders/",
        "aws_s3_trusted_order_statuses_path": s3_trusted_path + "order_statuses/",
    }


def add_partition_columns_in_df(
    df: pyspark.sql.dataframe.DataFrame, 
    col_used_to_partition: str,
    partition_cols_suffix: str = "",
):
    return (
        df
        .withColumn(f"partitioned_year{partition_cols_suffix}", year(col_used_to_partition))
        .withColumn(f"partitioned_month{partition_cols_suffix}", month(col_used_to_partition))
        .withColumn(f"partitioned_day{partition_cols_suffix}", dayofmonth(col_used_to_partition))
    )


def load_df_in_s3(
    partitioned_df: pyspark.sql.dataframe.DataFrame,
    dest_path: str,
    mode: str = "overwrite",
    partition_cols_suffix: str = "",
):
    try: 
        (
            partitioned_df
            .write.partitionBy(
                f"partitioned_year{partition_cols_suffix}", 
                f"partitioned_month{partition_cols_suffix}", 
                f"partitioned_day{partition_cols_suffix}"
             )
            .mode(mode)
            .parquet(dest_path)
        )
        
        return f"Success on saving {partitioned_df} on {dest_path}"
        
    except Exception as e:
        print(f"Error uploading df to S3. Error was: {e}")


def anonymize_df(df_to_anonymize):
    return (
        pyspark_anonymizer
        .Parser(
            df_to_anonymize,
            _anonymizer_configs(),
            spark_functions
        )
        .parse()
    )


def _anonymizer_configs():
    return [
    {
        "method": "sha256",
        "parameters": {
            "column_name": "cpf"
        }
    },
    {
        "method": "sha256",
        "parameters": {
            "column_name": "customer_name"
        }
    },
]


sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", get_aws_params()["aws_access_key"])
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", get_aws_params()["aws_secret_key"])

spark=SparkSession(sc)

raw_orders = spark.read.parquet(get_aws_params()["aws_s3_raw_order_path"])

stage_order = (
    raw_orders
    .withColumn("Time", col("order_created_at").cast("Timestamp"))
    .withColumn(
        "order_created_at_UTC", 
        to_utc_timestamp(col("Time"), col("merchant_timezone"))
    )
    .drop("Time")
)


anonimyzed_order = anonymize_df(stage_order)


load_df_in_s3(
    add_partition_columns_in_df(
        anonimyzed_order, 
        "order_created_at_UTC",
        partition_cols_suffix="_UTC"
    ),
    get_aws_params()["aws_s3_stage_order_path"],
    partition_cols_suffix="_UTC",
)

