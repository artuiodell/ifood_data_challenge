import pyspark

from pyspark.sql import SparkSession


def get_aws_params():
    s3_datalake_path = "s3://ifood-challenge-arthur-datalake/"
    
    s3_raw_path = s3_datalake_path + "raw/"
    s3_stage_path = s3_datalake_path + "stage/"
    s3_trusted_path = s3_datalake_path + "trusted/"
    
    return {
        "aws_access_key": "AKIAVF4EEUDKTPDXRA7Y",
        "aws_secret_key": "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",
        "aws_s3_raw_consumer_path": s3_raw_path + "consumer/",
        "aws_s3_raw_order_path": s3_raw_path + "order/",
        "aws_s3_raw_restaurant_path": s3_raw_path + "restaurant/",
        "aws_s3_raw_status_path": s3_raw_path + "status/",
        "aws_s3_stage_order_path": s3_stage_path + "stage_order/",
        "aws_s3_trusted_item_path": s3_trusted_path + "item/",
        "aws_s3_trusted_orders_path": s3_trusted_path + "orders/",
        "aws_s3_trusted_order_statuses_path": s3_trusted_path + "order_statuses/",
    }


def add_partition_columns_in_df(
    df: pyspark.sql.dataframe.DataFrame, 
    col_used_to_partition: str,
    partition_cols_suffix: str = "",
):
    return (
        df
        .withColumn(f"partitioned_year{partition_cols_suffix}", year(col_used_to_partition))
        .withColumn(f"partitioned_month{partition_cols_suffix}", month(col_used_to_partition))
        .withColumn(f"partitioned_day{partition_cols_suffix}", dayofmonth(col_used_to_partition))
    )


def load_df_in_s3(
    partitioned_df: pyspark.sql.dataframe.DataFrame,
    dest_path: str,
    mode: str = "overwrite",
    partition_cols_suffix: str = "",
):
    try: 
        (
            partitioned_df
            .write.partitionBy(
                f"partitioned_year{partition_cols_suffix}", 
                f"partitioned_month{partition_cols_suffix}", 
                f"partitioned_day{partition_cols_suffix}"
             )
            .mode(mode)
            .parquet(dest_path)
        )
        
        return f"Success on saving {partitioned_df} on {dest_path}"
        
    except Exception as e:
        print(f"Error uploading df to S3. Error was: {e}")


sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", get_aws_params()["aws_access_key"])
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", get_aws_params()["aws_secret_key"])

spark=SparkSession(sc)


raw_consumer = spark.read.parquet(get_aws_params()["aws_s3_raw_consumer_path"])
raw_restaurant = (
    spark
    .read
    .parquet(get_aws_params()["aws_s3_raw_restaurant_path"])
    .withColumnRenamed("id", "merchant_id")
)

stage_orders = spark.read.parquet(get_aws_params()["aws_s3_stage_order_path"])

trusted_order_statuses = spark.read.parquet(get_aws_params()["aws_s3_trusted_order_statuses_path"])


order_id_and_last_order = trusted_order_statuses.select("order_id", "last_order_status")


stage_order_with_last_order_status = (
    stage_orders
    .join(order_id_and_last_order, ["order_id"])
    .select(
        stage_orders["*"],
        order_id_and_last_order["last_order_status"],
    )
)

trusted_order = (
    stage_order_with_last_order_status
    .join(raw_consumer, ["customer_id"], "left")
    .join(raw_restaurant, ["merchant_id"], "left")
    .select(
        stage_order_with_last_order_status["*"],
        raw_consumer["language"],
        raw_consumer["active"].alias("is_customer_active"),
        raw_consumer["customer_phone_area"],
        raw_consumer["customer_phone_number"],
        raw_restaurant["created_at"],
        raw_restaurant["enabled"].alias("merchant_enabled"),
        raw_restaurant["price_range"].alias("merchant_price_range"),
        raw_restaurant["average_ticket"].alias("merchant_average_ticket"),
        raw_restaurant["takeout_time"].alias("merchant_takeout_time"),
        raw_restaurant["delivery_time"].alias("merchant_delivery_time"),
        raw_restaurant["minimum_order_value"].alias("merchant_minimum_order_value"),
        raw_restaurant["merchant_zip_code"],
        raw_restaurant["merchant_city"],
        raw_restaurant["merchant_state"],
        raw_restaurant["merchant_country"],
    )
    .drop("items", "partitioned_year", "partitioned_month", "partitioned_day")
)


load_df_in_s3(
    trusted_order,
    get_aws_params()["aws_s3_trusted_orders_path"],
    partition_cols_suffix="_UTC",
)
