import pyspark

from pyspark.sql import SparkSession
from pyspark.sql.functions import explode, col, from_json
from pyspark.sql.types import StructType, StructField, StringType, ArrayType


def get_aws_params():
    s3_datalake_path = "s3://ifood-challenge-arthur-datalake/"
    
    s3_raw_path = s3_datalake_path + "raw/"
    s3_stage_path = s3_datalake_path + "stage/"
    s3_trusted_path = s3_datalake_path + "trusted/"
    
    return {
        "aws_access_key": "AKIAVF4EEUDKTPDXRA7Y",
        "aws_secret_key": "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",
        "aws_s3_raw_consumer_path": s3_raw_path + "consumer/",
        "aws_s3_raw_order_path": s3_raw_path + "order/",
        "aws_s3_raw_restaurant_path": s3_raw_path + "restaurant/",
        "aws_s3_raw_status_path": s3_raw_path + "status/",
        "aws_s3_stage_order_path": s3_stage_path + "stage_order/",
        "aws_s3_trusted_item_path": s3_trusted_path + "item/",
        "aws_s3_trusted_orders_path": s3_trusted_path + "orders/",
        "aws_s3_trusted_order_statuses_path": s3_trusted_path + "order_statuses/",
    }


def add_partition_columns_in_df(
    df: pyspark.sql.dataframe.DataFrame, 
    col_used_to_partition: str,
    partition_cols_suffix: str = "",
):
    return (
        df
        .withColumn(f"partitioned_year{partition_cols_suffix}", year(col_used_to_partition))
        .withColumn(f"partitioned_month{partition_cols_suffix}", month(col_used_to_partition))
        .withColumn(f"partitioned_day{partition_cols_suffix}", dayofmonth(col_used_to_partition))
    )


def load_df_in_s3(
    partitioned_df: pyspark.sql.dataframe.DataFrame,
    dest_path: str,
    mode: str = "overwrite",
    partition_cols_suffix: str = "",
):
    try: 
        (
            partitioned_df
            .write.partitionBy(
                f"partitioned_year{partition_cols_suffix}", 
                f"partitioned_month{partition_cols_suffix}", 
                f"partitioned_day{partition_cols_suffix}"
             )
            .mode(mode)
            .parquet(dest_path)
        )
        
        return f"Success on saving {partitioned_df} on {dest_path}"
        
    except Exception as e:
        print(f"Error uploading df to S3. Error was: {e}")


sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", get_aws_params()["aws_access_key"])
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", get_aws_params()["aws_secret_key"])

spark=SparkSession(sc)

stage_order = spark.read.parquet(get_aws_params()["aws_s3_stage_order_path"])

stage_order_items = stage_order.select(
    "items",
    "order_id",
    "partitioned_year_UTC", 
    "partitioned_month_UTC", 
    "partitioned_day_UTC"
)

items_schema = ArrayType(
    StructType([
        StructField(
            "addition", 
             StructType([
               StructField("currency", StringType()),
               StructField("value", StringType()),  
            ])
        ), 
        StructField(
            "customerNote", StringType()
        ),
        StructField(
            "discount", 
            StructType([
               StructField("currency", StringType()),
               StructField("value", StringType()),  
            ])
        ),
        StructField(
            "externalId", StringType()
        ),
        StructField(
            "integrationId", StringType()
        ),
        StructField(
            "name", StringType()
        ),
        StructField(
            "quantity", StringType()
        ),
        StructField(
            "sequence", StringType()
        ),
        StructField(
            "totalAddition", 
            StructType([
               StructField("currency", StringType()),
               StructField("value", StringType()),  
            ])
        ),
        StructField(
            "totalDiscount",
            StructType([
               StructField("currency", StringType()),
               StructField("value", StringType()),  
            ])
        ),
        StructField(
            "totalValue", 
            StructType([
               StructField("currency", StringType()),
               StructField("value", StringType()),  
            ])
        ),
        StructField(
            "unitPrice",
            StructType([
               StructField("currency", StringType()),
               StructField("value", StringType()),  
            ])
        ),
        StructField(
            "garnishItems", 
            StringType(),
        ),
    ])
)

garnish_items_schema = ArrayType(StructType([
         StructField("name", StringType()),
         StructField("addition", StructType([
             StructField("currency", StringType()),
             StructField("value", StringType()),  
         ])),
         StructField("discount", StructType([
             StructField("currency", StringType()),
             StructField("value", StringType()),  
         ])),
         StructField("quantity", StringType()),  
         StructField("sequence", StringType()),  
         StructField("unitPrice", StructType([
             StructField("currency", StringType()),
             StructField("value", StringType()),  
         ])),  
         StructField("categoryId", StringType()),  
         StructField("externalId", StringType()), 
         StructField("totalValue", StructType([
             StructField("currency", StringType()),
             StructField("value", StringType()),  
         ])), 
         StructField("categoryName", StringType()), 
         StructField("integrationId", StringType()), 
        ]
))


stage_order_items_with_schema = stage_order_items.withColumn(
    "items", 
    from_json(stage_order_items.items, items_schema)
)


exploded_items_df = stage_order_items_with_schema.select(
    "order_id",
    "partitioned_year_UTC",
    "partitioned_month_UTC",
    "partitioned_day_UTC",
    explode(stage_order_items_with_schema.items)
)


trusted_order_items = (
    exploded_items_df
    .withColumn("customerNote", col("col.customerNote"))
    .withColumn("integrationId", col("col.integrationId"))
    .withColumn("externalId", col("col.externalId"))
    .withColumn("name", col("col.name"))
    .withColumn("quantity", col("col.quantity"))
    .withColumn("sequence", col("col.sequence"))
    .withColumn("addition_value", col("col.addition.value"))
    .withColumn("addition_currency", col("col.addition.currency"))
    .withColumn("discount_value", col("col.discount.value"))
    .withColumn("discount_currency", col("col.discount.currency"))
    .withColumn("totalAddition_value", col("col.totalAddition.value"))
    .withColumn("totalAddition_currency", col("col.totalAddition.currency"))    
    .withColumn("totalValue_value", col("col.totalValue.value"))
    .withColumn("totalValue_currency", col("col.totalValue.currency"))
    .withColumn("totalDiscount_value", col("col.totalDiscount.value"))
    .withColumn("totalDiscount_currency", col("col.totalDiscount.currency"))
    .withColumn("unitPrice_value", col("col.unitPrice.value"))
    .withColumn("unitPrice_currency", col("col.unitPrice.currency"))
    .withColumn("garnishItems", col("col.garnishItems"))
)


trusted_order_items = (
    trusted_order_items
    .withColumn(
        "garnishItems_array",
        from_json(trusted_order_items.garnishItems, garnish_items_schema)
    )
    .drop("col")
    .drop("garnishItems")
    .withColumnRenamed("garnishItems_array", "garnishItems")
)


load_df_in_s3(
    trusted_order_items,
    get_aws_params()["aws_s3_trusted_item_path"],
    partition_cols_suffix="_UTC",
)
