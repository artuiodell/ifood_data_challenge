import pyspark

from pyspark.sql import SparkSession
from pyspark.sql.functions import year, month, dayofmonth


def get_aws_params():
    s3_datalake_path = "s3://ifood-challenge-arthur-datalake/"
    
    s3_raw_path = s3_datalake_path + "raw/"
    s3_stage_path = s3_datalake_path + "stage/"
    s3_trusted_path = s3_datalake_path + "trusted/"
    
    return {
        "aws_access_key": "AKIAVF4EEUDKTPDXRA7Y",
        "aws_secret_key": "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",
        "aws_s3_raw_consumer_path": s3_raw_path + "consumer/",
        "aws_s3_raw_order_path": s3_raw_path + "order/",
        "aws_s3_raw_restaurant_path": s3_raw_path + "restaurant/",
        "aws_s3_raw_status_path": s3_raw_path + "status/",
        "aws_s3_stage_order_path": s3_stage_path + "stage_order/",
        "aws_s3_trusted_item_path": s3_trusted_path + "item/",
        "aws_s3_trusted_orders_path": s3_trusted_path + "orders/",
        "aws_s3_trusted_order_statuses_path": s3_trusted_path + "order_statuses/",
    }


def add_partition_columns_in_df(
    df: pyspark.sql.dataframe.DataFrame, 
    col_used_to_partition: str,
    partition_cols_suffix: str = "",
):
    return (
        df
        .withColumn(f"partitioned_year{partition_cols_suffix}", year(col_used_to_partition))
        .withColumn(f"partitioned_month{partition_cols_suffix}", month(col_used_to_partition))
        .withColumn(f"partitioned_day{partition_cols_suffix}", dayofmonth(col_used_to_partition))
    )


def load_df_in_s3(
    partitioned_df: pyspark.sql.dataframe.DataFrame,
    dest_path: str,
    mode: str = "overwrite",
    partition_cols_suffix: str = "",
):
    try: 
        (
            partitioned_df
            .write.partitionBy(
                f"partitioned_year{partition_cols_suffix}", 
                f"partitioned_month{partition_cols_suffix}", 
                f"partitioned_day{partition_cols_suffix}"
             )
            .mode(mode)
            .parquet(dest_path)
        )
        
        return f"Success on saving {partitioned_df} on {dest_path}"
        
    except Exception as e:
        print(f"Error uploading df to S3. Error was: {e}")


orders_json_path_0 = "s3://ifood-data-architect-test-source/orders/part-000.json.gz"
orders_json_path_1 = "s3://ifood-data-architect-test-source/orders/part-001.json.gz"
orders_json_path_2 = "s3://ifood-data-architect-test-source/orders/part-002.json.gz"
orders_json_path_3 = "s3://ifood-data-architect-test-source/orders/part-003.json.gz"
orders_json_path_4 = "s3://ifood-data-architect-test-source/orders/part-004.json.gz"
orders_json_path_5 = "s3://ifood-data-architect-test-source/orders/part-005.json.gz"
orders_json_path_6 = "s3://ifood-data-architect-test-source/orders/part-006.json.gz"
orders_json_path_7 = "s3://ifood-data-architect-test-source/orders/part-007.json.gz"
orders_json_path_8 = "s3://ifood-data-architect-test-source/orders/part-008.json.gz"
orders_json_path_9 = "s3://ifood-data-architect-test-source/orders/part-009.json.gz"


status_json_path = "s3://ifood-data-architect-test-source/status.json.gz"
restaurant_csv_path = "s3://ifood-data-architect-test-source/restaurant.csv.gz"
consumer_csv_path = "s3://ifood-data-architect-test-source/consumer.csv.gz"


sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", get_aws_params()["aws_access_key"])
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", get_aws_params()["aws_secret_key"])

spark=SparkSession(sc)


consumer_df = spark.read.csv(consumer_csv_path, header='true')
restaurant_df = spark.read.csv(restaurant_csv_path, header='true')
status_df = spark.read.json(status_json_path)

orders_df_0 = spark.read.json(orders_json_path_0)
orders_df_1 = spark.read.json(orders_json_path_1)
orders_df_2 = spark.read.json(orders_json_path_2)
orders_df_3 = spark.read.json(orders_json_path_3)
orders_df_4 = spark.read.json(orders_json_path_4)
orders_df_5 = spark.read.json(orders_json_path_5)
orders_df_6 = spark.read.json(orders_json_path_6)
orders_df_7 = spark.read.json(orders_json_path_7)
orders_df_8 = spark.read.json(orders_json_path_8)
orders_df_9 = spark.read.json(orders_json_path_9)

orders_df = (
    orders_df_0
    .union(orders_df_1)
    .union(orders_df_2)
    .union(orders_df_3)
    .union(orders_df_4)
    .union(orders_df_5)
    .union(orders_df_6)
    .union(orders_df_7)
    .union(orders_df_8)
    .union(orders_df_9)
)


dfs_list = [
    {
        "name": consumer_df,
        "col_used_to_partition": "created_at",
        "path": get_aws_params()["aws_s3_raw_consumer_path"]
    },
    {
        "name": restaurant_df,
        "col_used_to_partition": "created_at",
        "path": get_aws_params()["aws_s3_raw_restaurant_path"]
    },
    {
        "name": status_df,
        "col_used_to_partition": "created_at",
        "path": get_aws_params()["aws_s3_raw_status_path"]
    },
    {
        "name": orders_df,
        "col_used_to_partition": "order_created_at",
        "path": get_aws_params()["aws_s3_raw_order_path"]
    },
] 

for df in dfs_list:
    load_df_in_s3(
        add_partition_columns_in_df(df["name"], df["col_used_to_partition"]),
        df["path"],
    )
