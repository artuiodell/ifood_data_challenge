import pyspark

from pyspark.sql import SparkSession
from pyspark.sql.functions import expr


def get_aws_params():
    s3_datalake_path = "s3://ifood-challenge-arthur-datalake/"
    
    s3_raw_path = s3_datalake_path + "raw/"
    s3_stage_path = s3_datalake_path + "stage/"
    s3_trusted_path = s3_datalake_path + "trusted/"
    
    return {
        "aws_access_key": "AKIAVF4EEUDKTPDXRA7Y",
        "aws_secret_key": "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",
        "aws_s3_raw_consumer_path": s3_raw_path + "consumer/",
        "aws_s3_raw_order_path": s3_raw_path + "order/",
        "aws_s3_raw_restaurant_path": s3_raw_path + "restaurant/",
        "aws_s3_raw_status_path": s3_raw_path + "status/",
        "aws_s3_stage_order_path": s3_stage_path + "stage_order/",
        "aws_s3_trusted_item_path": s3_trusted_path + "item/",
        "aws_s3_trusted_orders_path": s3_trusted_path + "orders/",
        "aws_s3_trusted_order_statuses_path": s3_trusted_path + "order_statuses/",
    }

def add_partition_columns_in_df(
    df: pyspark.sql.dataframe.DataFrame, 
    col_used_to_partition: str,
    partition_cols_suffix: str = "",
):
    return (
        df
        .withColumn(f"partitioned_year{partition_cols_suffix}", year(col_used_to_partition))
        .withColumn(f"partitioned_month{partition_cols_suffix}", month(col_used_to_partition))
        .withColumn(f"partitioned_day{partition_cols_suffix}", dayofmonth(col_used_to_partition))
    )


def load_df_in_s3(
    partitioned_df: pyspark.sql.dataframe.DataFrame,
    dest_path: str,
    mode: str = "overwrite",
    partition_cols_suffix: str = "",
):
    try: 
        (
            partitioned_df
            .write.partitionBy(
                f"partitioned_year{partition_cols_suffix}", 
                f"partitioned_month{partition_cols_suffix}", 
                f"partitioned_day{partition_cols_suffix}"
             )
            .mode(mode)
            .parquet(dest_path)
        )
        
        return f"Success on saving {partitioned_df} on {dest_path}"
        
    except Exception as e:
        print(f"Error uploading df to S3. Error was: {e}")


sc._jsc.hadoopConfiguration().set("fs.s3n.awsAccessKeyId", get_aws_params()["aws_access_key"])
sc._jsc.hadoopConfiguration().set("fs.s3n.awsSecretAccessKey", get_aws_params()["aws_secret_key"])

spark=SparkSession(sc)


raw_status = spark.read.parquet(get_aws_params()["aws_s3_raw_status_path"])
stage_orders = spark.read.parquet(get_aws_params()["aws_s3_stage_order_path"])

distinct_raw_status = raw_status.select("order_id", "value", "created_at").distinct()

status_with_order_values_timestamps = distinct_raw_status.select(
    "order_id",
    expr("CASE WHEN `value` = 'PLACED' THEN `created_at` ELSE null END as value_placed_timestamp"),
    expr("CASE WHEN `value` = 'CONCLUDED' THEN `created_at` ELSE null END as value_concluded_timestamp"),
    expr("CASE WHEN `value` = 'REGISTERED' THEN `created_at` ELSE null END as value_registered_timestamp"),
    expr("CASE WHEN `value` = 'CANCELLED' THEN `created_at` ELSE null END as value_cancelled_timestamp"),
)



placed_df = (
    status_with_order_values_timestamps
    .select("order_id", "value_placed_timestamp")
    .filter(status_with_order_values_timestamps.value_placed_timestamp != "null")
)

concluded_df = (
    status_with_order_values_timestamps
    .select("order_id", "value_concluded_timestamp")
    .filter(status_with_order_values_timestamps.value_concluded_timestamp != "null")
)

registered_df = (
    status_with_order_values_timestamps
    .select("order_id", "value_registered_timestamp")
    .filter(status_with_order_values_timestamps.value_registered_timestamp != "null")
)

cancelled_df = (
    status_with_order_values_timestamps
    .select("order_id", "value_cancelled_timestamp")
    .filter(status_with_order_values_timestamps.value_cancelled_timestamp != "null")
)

distinct_order_ids = (
    status_with_order_values_timestamps
    .select("order_id").distinct()
)

join_all_dfs = (
    distinct_order_ids
    .join(placed_df, "order_id", "left")
    .join(concluded_df, "order_id", "left")
    .join(registered_df, "order_id", "left")
    .join(cancelled_df, "order_id", "left")
    .select(
        distinct_order_ids["*"],
        placed_df["value_placed_timestamp"],
        concluded_df["value_concluded_timestamp"],
        registered_df["value_registered_timestamp"],
        cancelled_df["value_cancelled_timestamp"]
    )
)

stage_order_statuses = join_all_dfs.select(
    "*",
    expr("""CASE WHEN 
               (
                   `value_placed_timestamp` > `value_concluded_timestamp`
                   or `value_concluded_timestamp` is null
                )
               and (
                   `value_placed_timestamp` > `value_registered_timestamp`
                   or `value_registered_timestamp` is null
                )
               and (
                   `value_placed_timestamp` > `value_cancelled_timestamp`
                   or `value_cancelled_timestamp` is null
                )
               THEN 'PLACED'
            WHEN 
               (
                   `value_concluded_timestamp` > `value_placed_timestamp`
                   or `value_placed_timestamp` is null
               )
               and (
                   `value_concluded_timestamp` > `value_registered_timestamp`
                   or `value_registered_timestamp` is null
               )
               and (
                   `value_concluded_timestamp` > `value_cancelled_timestamp`
                   or `value_cancelled_timestamp` is null
               )
               THEN 'CONCLUDED'
            WHEN 
               (
                   `value_registered_timestamp` > `value_placed_timestamp`
                   or `value_placed_timestamp` is null
               )
               and (
                   `value_registered_timestamp` > `value_concluded_timestamp` 
                   or `value_concluded_timestamp` is null
               )
               and (
                   `value_registered_timestamp` > `value_cancelled_timestamp` 
                   or `value_cancelled_timestamp` is null
               )
               THEN 'REGISTERED'
            WHEN 
               (
                   `value_cancelled_timestamp` > `value_placed_timestamp`
                   or `value_placed_timestamp` is null
               )
               and (
                   `value_cancelled_timestamp` > `value_concluded_timestamp` 
                   or `value_concluded_timestamp` is null
               )
               and (
                   `value_cancelled_timestamp` > `value_registered_timestamp`
                   or `value_registered_timestamp` is null
               )
               THEN 'CANCELLED'
           ELSE 'undefined' END  as last_order_status
        """)
)

trusted_order_statuses = (
    stage_order_statuses
    .join(stage_orders,  ["order_id"])
    .select(
        stage_order_statuses["*"],
        stage_orders["partitioned_year_UTC"],
        stage_orders["partitioned_month_UTC"],
        stage_orders["partitioned_day_UTC"],
    )
    .distinct()
)

load_df_in_s3(
    trusted_order_statuses,
    get_aws_params()["aws_s3_trusted_order_statuses_path"],
    partition_cols_suffix="_UTC",
)
